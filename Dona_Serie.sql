INSERT INTO simplonflix.Serie(id_serie, serie_type, serie_name) VALUES (4, "Action_Drame", "Arrow");

INSERT INTO simplonflix.Season(id_season, season_nb, id_serie) VALUES (4, 4, 4);

SELECT * from simplonflix.Episode;
INSERT INTO simplonflix.Episode(id_episode, director, episode_name, actors, episode_nb, runtime, id_season)
 VALUES (31, "James Bamford", "Green Arrow", "Stephen Amell", 1, "00:42:00", 4),
		(32, "John Behring", "The Candidate", "Emily Bett Rickards", 2, "00:44:00", 4),
		(33, "Wendey Stanzler", "Restoration", "Katie Cassidy", 3, "00:41:00", 4),
        (34, "Lexi Alexander", "Beyond Redemption", "Willa Holland ", 4, "00:46:00", 4),
        (35, "John Badham", "Haunted", "John Barrowman", 5, "00:44:00", 4),
        (36, "Antonio Negret", "Lost Souls", "Paul Blackthorne", 6, "00:43:00", 4),
        (37, "James Bamford", "Brotherhood", "Audrey Marie Anderson", 7, "00:42:00", 4),
        (38, "Thor Freudenthal", "Legends of Yesterday", "Neal McDonough", 8, "00:40:00", 4),
        (39, "John Behring", "Dark Waters", "Caity Lotz", 9, "00:45:00", 4),
        (40, "Jesse Warn", "Blood Debts", "Ryan Robbins", 10, "00:49:00", 4);






