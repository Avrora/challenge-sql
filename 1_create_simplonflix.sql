CREATE DATABASE simplonflix;
use simplonflix;

CREATE TABLE simplonflix.Serie (
	id_serie INT,
    serie_type VARCHAR(200),
    serie_name VARCHAR(100),
        PRIMARY KEY (id_serie)
);
CREATE TABLE simplonflix.Season (
	id_season INT,
    season_nb INT,
    id_serie INT,
		PRIMARY KEY(id_season),
        FOREIGN KEY(id_serie)
			REFERENCES Serie (id_serie)
);
CREATE TABLE simplonflix.Episode (
    id_episode INT,
    director VARCHAR(100),
    episode_name VARCHAR(100),
    actors VARCHAR(100),
    episode_nb INT,
    runtime TIME,
    id_season INT,
    PRIMARY KEY (id_episode),
    FOREIGN KEY (id_season)
        REFERENCES Season (id_season)
);