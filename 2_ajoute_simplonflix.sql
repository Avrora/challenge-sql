INSERT INTO simplonflix.Serie(id_serie, serie_type, serie_name) VALUES (3, "Science fiction", "Black mirror");

INSERT INTO simplonflix.Season(id_season, season_nb, id_serie) VALUES (3, 3, 3);

INSERT INTO simplonflix.Episode(id_episode, director, episode_name, actors, episode_nb, runtime, id_season)
 VALUES (21, "Joe Wright", "Nosedive", "Bryce Dallas Howard", 1, "01:00:58", 3),
		(22, "James Watkins", "Shut up and dance", "Alex Lawther", 3, "01:01:45", 3);